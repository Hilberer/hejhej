import React from 'react'
import { Footer } from '../components/Footer'
import { Box } from '@chakra-ui/react'

export const Home = () => {
    return (
        <Box>
            Welcome!
            <Footer/>
        </Box>
    )
}
