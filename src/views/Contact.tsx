import React from 'react'
import { Box } from '@chakra-ui/react'
import { Footer } from '../components/Footer'

export const Contact = () => {
    return (
        <Box>
            Kontakta oss!
            <Footer />
        </Box>
    )
}
