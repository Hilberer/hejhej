import React from 'react'
import { Stack, Input, Textarea, Button } from '@chakra-ui/react'

export const Footer = () => {
    return (
        <Stack >
                <Input placeholder='Namn'/>
                <Input placeholder='Email'/>
                <Textarea placeholder='Ställ din fråga' />
                <Button>Skicka</Button>
        </Stack>
    )
}
