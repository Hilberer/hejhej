import React from 'react'
import { Flex, Spacer, Heading, Box } from '@chakra-ui/react'
import { HamburgerMenu } from './HamburgerMenu'

export const Navigation = () => {
    return (
        <Flex bg='yellow.300' borderBottom='2px'>
            <Box p='2'>
                <Heading size='sm'>3 Ekonomer</Heading>
            </Box>
            <Spacer/>
            <Box>
                <HamburgerMenu/>
            </Box>
        </Flex>
    )
}
