import React from 'react'
import { TiThMenu } from 'react-icons/ti'
import { Link as RouterLink } from 'react-router-dom'
import { useState } from 'react'
import { BiX } from "react-icons/bi"

import {
    Menu,
    MenuButton,
    MenuList,
    MenuItem,
    IconButton,
    Link
  } from '@chakra-ui/react'
import RoutingPath from '../routes/RoutingPath'

export const HamburgerMenu = () => {
    const [show, setShow] = useState(true)
    const handleClick = () => setShow(!show)
    return (
        <Menu>
            <MenuButton
                onClick={handleClick}
                backgroundColor='yellow.300'
                as={IconButton}
                aria-label='Options'
                icon={show ? <TiThMenu/> : <BiX/>}
                variant='outline'
                borderColor='blackAlpha.400'
                borderWidth='1px'
                borderRightRadius="0"
                _expanded={{ bg: 'yellow.200' }}
                _focus={{ focus: 'none' }}
                />
            <MenuList>
                <MenuItem>
                    <Link width='100%' as={RouterLink} to={RoutingPath.home}>Home</Link>
                </MenuItem>
                <MenuItem>
                    <Link width='100%' as={RouterLink} to={RoutingPath.tjanster}>Tjänster</Link>
                </MenuItem>
                <MenuItem>
                    <Link width='100%' as={RouterLink} to={RoutingPath.contact}>Kontakta oss</Link>
                </MenuItem>
            </MenuList>
        </Menu>
    )
}
