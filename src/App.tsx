import * as React from "react"
import { Routes } from './routes/Routes'
import { Navigation } from './components/Navigation'
import {
  ChakraProvider,
  theme,
} from "@chakra-ui/react"

export const App = () => {
  return (
    <ChakraProvider theme={theme}>
      <Routes>
        <Navigation/>
      </Routes>
    </ChakraProvider>
  )
}
