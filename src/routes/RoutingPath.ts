const home = '/home'
const tjanster = '/tjanster'
const contact = '/kontakt'

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    home,
    tjanster,
    contact
}