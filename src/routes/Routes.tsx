import React from 'react'
import { Home } from '../views/Home'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { Tjanster } from '../views/Tjanster'
import { Contact } from '../views/Contact'
import RoutingPath from './RoutingPath'

export const Routes = (props: { children: React.ReactChild }) => {
    const { children } = props
    return (
        <BrowserRouter>
        {children}
            <Switch>
                <Route exact path={RoutingPath.home} component={Home} />
                <Route exact path={RoutingPath.tjanster} component={Tjanster} />
                <Route exact path={RoutingPath.contact} component={Contact} />
            </Switch>
        </BrowserRouter>
    )
}
